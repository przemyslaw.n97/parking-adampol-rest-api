create table car
(
    id           INTEGER AUTO_INCREMENT PRIMARY KEY,
    plate_number VARCHAR(255)
);

create table parking
(
    id   INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);

create table place
(
    id         INTEGER AUTO_INCREMENT PRIMARY KEY,
    name       VARCHAR(255),
    car_id     INTEGER REFERENCES car (id),
    parking_id INTEGER NOT NULL REFERENCES parking (id)
);

INSERT INTO car (id, plate_number)
VALUES (1, 'asdasd'),
       (2, 'asdfsdfasd');

INSERT INTO parking(id, name) VALUES (1, 'adsf');

INSERT INTO place(id, name, car_id, parking_id) VALUES (1, 'asd', 1, 1);