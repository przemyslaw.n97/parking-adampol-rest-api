package pl.pn.adampol.parking;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.pn.adampol.place.Place;
import pl.pn.adampol.place.PlaceResponse;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ParkingService {

    protected final ParkingRepository parkingRepository;

    List<PlaceResponse> findAvailablePlaces(long parkingId) {
        return parkingRepository.findFreePlaces(parkingId)
                .stream()
                .map(Place::toResponse)
                .collect(Collectors.toList());
    }

    protected Parking createEntity(ParkingRequest request) {
        return parkingRepository.save(Parking.from(request));
    }

    Optional<ParkingResponse> update(ParkingRequest parkingRequest, Long id) {
        if (parkingRepository.existsById(id)) {
            return Optional.of(parkingRepository.save(Parking.from(parkingRequest)).toResponse());
        } else {
            return Optional.empty();
        }
    }

    Optional<ParkingResponse> findOne(long id) {
        return parkingRepository.findById(id).map(Parking::toResponse);
    }

    Page<ParkingResponse> findAll(Pageable pageable) {
        return parkingRepository.findAll(pageable).map(Parking::toResponse);
    }

    void delete(long id) {
        if (parkingRepository.existsById(id)) {
            parkingRepository.deleteById(id);
        }
    }
}
