package pl.pn.adampol.parking;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pn.adampol.place.PlaceService;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
class ParkingFacade extends ParkingService {

    private final PlaceService placeService;
    private final EntityManager manager;

    public ParkingFacade(PlaceService placeService, ParkingRepository repository, EntityManager manager) {
        super(repository);
        this.placeService = placeService;
        this.manager = manager;
    }

    @Transactional
    ParkingResponse create(ParkingRequest request) {
        final Parking parking = super.createEntity(request);

        placeService.createAll(request.getPlaces(), parking);

        manager.clear();
        return parkingRepository.getOne(parking.getId()).toResponse();
    }

    @Transactional
    Optional<ParkingResponse> update(ParkingRequest parkingRequest, Long id) {
        if (parkingRepository.existsById(id)) {
            final Parking updatedParking = parkingRepository.save(Parking.from(parkingRequest));

            placeService.updateAll(parkingRequest.getPlaces(), updatedParking);

            manager.clear();
            return findOne(id);
        } else {
            return Optional.empty();
        }
    }

}
