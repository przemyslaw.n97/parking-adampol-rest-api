package pl.pn.adampol.parking;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.pn.adampol.place.PlaceResponse;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("parking")
@RequiredArgsConstructor
class ParkingController {

    private final ParkingFacade parkingFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ADMIN')")
    public ParkingResponse create(@RequestBody final ParkingRequest request) {
        log.info("[POST] /parking - Creating new parking - {}", request);

        return parkingFacade.create(request);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ParkingResponse> update(@RequestBody final ParkingRequest request, @PathVariable long id) {
        log.info("[PUT] /parking/{id} - Updating parking with id {}, request - {}", id, request);

        return ResponseEntity.of(parkingFacade.update(request, id));
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    public ResponseEntity<ParkingResponse> findOne(@PathVariable long id) {
        log.info("[GET] /parking{id} - Searing parking with id {}", id);

        return ResponseEntity.of(parkingFacade.findOne(id));
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    public Page<ParkingResponse> findAll(Pageable pageable) {
        log.info("[GET] /parking - Searching all parking");

        return parkingFacade.findAll(pageable);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) {
        log.info("[DELETE] /parking/{id} - Deleting parking with id {}", id);

        parkingFacade.delete(id);
    }

    @GetMapping("/{id}/places")
    public List<PlaceResponse> findAvailablePlaces(@PathVariable Long id) {
        log.info("[GET] /parking - Searching available places");

        return parkingFacade.findAvailablePlaces(id);
    }
}
