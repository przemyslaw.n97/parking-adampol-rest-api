package pl.pn.adampol.parking;

import lombok.Builder;
import lombok.Value;
import pl.pn.adampol.place.PlaceRequest;

import java.util.List;

@Builder
@Value
public class ParkingRequest {

    String name;
    List<PlaceRequest> places;
}
