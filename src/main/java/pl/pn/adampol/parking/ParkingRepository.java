package pl.pn.adampol.parking;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pl.pn.adampol.place.Place;

import java.util.List;

interface ParkingRepository extends JpaRepository<Parking, Long> {

    @Query("FROM Place place WHERE place.parking.id = :parkingId AND place.carId is null")
    List<Place> findFreePlaces(@Param("parkingId") long parkingId);
}
