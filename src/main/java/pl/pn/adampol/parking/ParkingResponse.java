package pl.pn.adampol.parking;

import lombok.Builder;
import lombok.Value;
import pl.pn.adampol.place.PlaceRequest;
import pl.pn.adampol.place.PlaceResponse;

import java.util.List;

@Builder
@Value
public class ParkingResponse {

    long id;
    String name;
    List<PlaceResponse> places;
}
