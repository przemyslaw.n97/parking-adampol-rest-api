package pl.pn.adampol.parking;

import lombok.Getter;
import pl.pn.adampol.place.Place;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Entity
@Getter
public class Parking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany(mappedBy = "parking", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Place> places;

    static Parking from(ParkingRequest request) {
        final Parking parking = new Parking();
        parking.name = request.getName();
        return parking;
    }

    ParkingResponse toResponse() {
        return ParkingResponse.builder()
                .id(id)
                .name(name)
                .places(nonNull(places) ? places.stream()
                        .map(Place::toResponse)
                        .collect(Collectors.toList()) : null )
                .build();
    }
}
