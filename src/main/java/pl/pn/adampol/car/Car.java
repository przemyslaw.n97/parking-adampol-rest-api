package pl.pn.adampol.car;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String plateNumber;

    static Car from(CreateOrUpdateCarRequest carRequest) {
        final Car car = new Car();
        car.plateNumber = carRequest.getPlateNumber();
        return car;
    }

    CarResponse toResponse() {
        return CarResponse.builder()
                .id(id)
                .plateNumber(plateNumber)
                .build();
    }
}
