package pl.pn.adampol.car;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateOrUpdateCarRequest {

    private String plateNumber;
}
