package pl.pn.adampol.car;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class CarResponse {

    long id;
    String plateNumber;
}
