package pl.pn.adampol.car;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@Slf4j
@RestController
@RequestMapping("cars")
@RequiredArgsConstructor
class CarController {

    private final CarService carService;

    @PostMapping
    @ResponseStatus(CREATED)
    @PreAuthorize("hasRole('ADMIN')")
    public CarResponse create(@RequestBody CreateOrUpdateCarRequest request) {
        log.info("[POST] /cars - Creating new car: {}", request.toString());

        return carService.create(request);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<CarResponse> update(@RequestBody CreateOrUpdateCarRequest request, @PathVariable Long id) {
        log.info("[PUT] /cars/{id} - Updating car with id: {}", id);

        return ResponseEntity.of(carService.update(request, id));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    public ResponseEntity<CarResponse> findOne(@PathVariable Long id) {
        log.info("[GET] /cars{id} - Searching car with id: {}", id);

        return ResponseEntity.of(carService.findOne(id));
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    public Page<CarResponse> finaAll(Pageable pageable) {
        log.info("[GET] /cars - Searching cars page: {}", pageable.getPageNumber());

        return carService.findAll(pageable);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable Long id) {
        log.info("[DELETE] /cars{id} - Delete car with id: {}", id);

        carService.delete(id);
    }
}
