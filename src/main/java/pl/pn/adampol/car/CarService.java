package pl.pn.adampol.car;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
class CarService {

    private final CarRepository carRepository;

    CarResponse create(CreateOrUpdateCarRequest request) {
        return carRepository.save(Car.from(request)).toResponse();
    }

    Optional<CarResponse> update(CreateOrUpdateCarRequest request, long id) {
        carRepository.update(id, request.getPlateNumber());

        return carRepository.findById(id).map(Car::toResponse);
    }

    Optional<CarResponse> findOne(long id) {
        return carRepository.findById(id).map(Car::toResponse);
    }

    Page<CarResponse> findAll(Pageable pageable) {
        return carRepository.findAll(pageable).map(Car::toResponse);
    }

    void delete(long id) {
        if (carRepository.existsById(id)) {
            carRepository.deleteById(id);
        }
    }
}
