package pl.pn.adampol.car;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

interface CarRepository extends JpaRepository<Car, Long> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE Car c SET " +
            "c.plateNumber = :plateNumber " +
            "WHERE c.id = :id")
    void update(@Param("id") long id, @Param("plateNumber") String plateNumber);
}
