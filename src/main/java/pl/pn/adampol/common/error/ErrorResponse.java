package pl.pn.adampol.common.error;

import lombok.Value;

import java.time.LocalDateTime;

@Value(staticConstructor = "of")
public class ErrorResponse {

    String message;
    LocalDateTime time = LocalDateTime.now();
}
