package pl.pn.adampol.place;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class PlaceResponse {

    Long id;
    String name;
    Long carId;
}
