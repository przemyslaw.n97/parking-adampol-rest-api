package pl.pn.adampol.place;

import lombok.Getter;
import pl.pn.adampol.parking.Parking;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
public class Place {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long carId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parking_id")
    private Parking parking;

    public static Place from(PlaceRequest request, Parking parking) {
        final Place place = new Place();
        place.id = request.getId();
        place.name = request.getName();
        place.carId = request.getCarId();
        place.parking = parking;
        return place;
    }

    public PlaceResponse toResponse() {
        return PlaceResponse.builder()
                .id(id)
                .name(name)
                .carId(carId)
                .build();
    }
}
