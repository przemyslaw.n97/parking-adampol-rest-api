package pl.pn.adampol.place;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class PlaceRequest {

    Long id;
    String name;
    Long carId;
}
