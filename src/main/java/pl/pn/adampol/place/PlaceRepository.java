package pl.pn.adampol.place;

import org.springframework.data.jpa.repository.JpaRepository;

interface PlaceRepository extends JpaRepository<Place, Long> {
}
