package pl.pn.adampol.place;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pn.adampol.parking.Parking;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaceService {

    private final PlaceRepository placeRepository;

    public void createAll(List<PlaceRequest> places, Parking parking) {
        places.forEach(place ->
                placeRepository.save(Place.from(place, parking)).toResponse());
    }

    public void updateAll(List<PlaceRequest> places, Parking parking) {
        places.forEach(place ->
                placeRepository.save(Place.from(place, parking)).toResponse());
    }
}
