package pl.pn.adampol.security;

import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.pn.adampol.common.error.ErrorResponse;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static pl.pn.adampol.security.SecurityConst.TOKEN_PREFIX;

@Slf4j
@RestController
@RequiredArgsConstructor
@PreAuthorize("permitAll()")
class UserController {

    private final UserService userService;
    private final UserFacade userFacade;

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody UserRequest userRequest) {
        log.info("[POST] /user/register - Register new user: {}", userRequest);

        final Either<ErrorResponse, UserResponse> eitherUser = userService.registryUser(userRequest);

        if (eitherUser.isRight()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(eitherUser.get());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(eitherUser.getLeft());
    }

    @PostMapping("/login")
    public ResponseEntity<Void> login(@RequestBody LoginRequest loginRequest) {
        log.info("[POST] /login - Login user {}", loginRequest.getLogin());

        final String token = userFacade.login(loginRequest);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).header(AUTHORIZATION, TOKEN_PREFIX + token).build();
    }
}
