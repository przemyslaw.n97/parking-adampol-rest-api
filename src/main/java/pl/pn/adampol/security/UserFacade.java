package pl.pn.adampol.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pl.pn.adampol.security.token.JwtTokenService;

@Service
@RequiredArgsConstructor
public class UserFacade {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsServiceImpl userService;
    private final JwtTokenService tokenService;

    String login(LoginRequest request) {
        final UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword());

        authenticationManager.authenticate(token);

        final UserDetails user = userService.loadUserByUsername(request.getLogin());
        return tokenService.generateToken(user);
    }
}
