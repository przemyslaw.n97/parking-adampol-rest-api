package pl.pn.adampol.security;

import lombok.Value;

@Value(staticConstructor = "of")
public class UserResponse {

    String login;
}