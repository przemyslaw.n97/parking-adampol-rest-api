package pl.pn.adampol.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String login;
    private String password;
    private boolean enabled;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    static UserEntity from(UserRequest userRequest) {
        final UserEntity userEntity = new UserEntity();
        userEntity.login = userRequest.getLogin();
        userEntity.password = userRequest.getPassword();
        userEntity.role = userRequest.getRole();
        return userEntity;
    }

    static UserEntity of(String login, String password, UserRole role) {
        final UserEntity userEntity = new UserEntity();
        userEntity.login = login;
        userEntity.password = password;
        userEntity.role = role;
        return userEntity;
    }
}
