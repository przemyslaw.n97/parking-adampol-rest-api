package pl.pn.adampol.security.config;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Service;
import pl.pn.adampol.security.UserDetailsServiceImpl;
import pl.pn.adampol.security.UserService;
import pl.pn.adampol.security.token.JwtTokenService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static pl.pn.adampol.security.SecurityConst.TOKEN_PREFIX;

public class AuthenticationTokenFilter extends BasicAuthenticationFilter {

    private final JwtTokenService jwtTokenService;
    private final UserDetailsService userService;

    public AuthenticationTokenFilter(JwtTokenService jwtTokenService, AuthenticationManager authenticationManager, UserDetailsServiceImpl userService) {
        super(authenticationManager);
        this.jwtTokenService = jwtTokenService;
        this.userService = userService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        final Optional<String> token = extractToken(request);
        token.ifPresent(this::authenticate);
        chain.doFilter(request, response);
    }

    private void authenticate(String token) {
        String login = jwtTokenService.extractLogin(token);

        final UserDetails user = userService.loadUserByUsername(login);

        if (!jwtTokenService.isTokenValid(token, user)) {
            throw new BadCredentialsException("Invalid token");
        }


        final UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                user.getUsername(), null, user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authToken);
    }

    private Optional<String> extractToken(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(AUTHORIZATION))
                .filter(header -> header.startsWith(TOKEN_PREFIX))
                .map(header -> header.replace(TOKEN_PREFIX, ""));

    }
}
