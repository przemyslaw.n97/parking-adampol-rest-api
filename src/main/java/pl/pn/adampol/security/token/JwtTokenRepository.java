package pl.pn.adampol.security.token;

import org.springframework.data.jpa.repository.JpaRepository;

interface JwtTokenRepository extends JpaRepository<JwtToken, String> {
}
