package pl.pn.adampol.security.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtToken {

    @Id
    private String content;
    private LocalDateTime expirationDate;
}
