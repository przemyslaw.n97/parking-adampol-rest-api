package pl.pn.adampol.security.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.function.Function;
import java.util.stream.Collectors;

import static pl.pn.adampol.security.SecurityConst.TOKEN_SCOPE;

@Service
@RequiredArgsConstructor
public class JwtTokenService {

    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;
    private static final SecretKey SECRET_KEY = Keys.secretKeyFor(SIGNATURE_ALGORITHM);
    private final JwtTokenRepository tokenRepository;

    public String generateToken(UserDetails user) {
        final String scope = user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(", "));

        final LocalDateTime expirationDateTime = LocalDateTime.now().plusHours(12);

        Function<LocalDateTime, Date> toDate =
                localDateTime -> Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

        final String token = Jwts.builder()
                .claim(TOKEN_SCOPE, scope)
                .setSubject(user.getUsername())
                .setIssuedAt(toDate.apply(LocalDateTime.now()))
                .setExpiration(toDate.apply(expirationDateTime))
                .signWith(SECRET_KEY, SIGNATURE_ALGORITHM)
                .compact();

        tokenRepository.save(new JwtToken(token, expirationDateTime));
        return token;
    }

    public String extractLogin(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public boolean isTokenValid(String token, UserDetails user) {
        final String login = extractLogin(token);
        return login.equals(user.getUsername()) && !isTokenExpired(token);
    }

    private <T> T extractClaim(String token, Function<Claims, T> claimResolver) {
        Claims claims = extractAllClaims(token);
        return claimResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parserBuilder().setSigningKey(SECRET_KEY).build().parseClaimsJws(token).getBody();
    }

    private boolean isTokenExpired(String token) {
        final Date expirationDate = extractExpiration(token);
        return expirationDate.before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
}
