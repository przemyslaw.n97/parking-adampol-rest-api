package pl.pn.adampol.security;

public enum UserRole {
    ADMIN,
    USER
}
