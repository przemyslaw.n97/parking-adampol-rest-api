package pl.pn.adampol.security;

import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.pn.adampol.common.error.ErrorResponse;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserValidator userValidator;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public Either<ErrorResponse, UserResponse> registryUser(UserRequest userRequest) {
        if (!userValidator.validatePassword(userRequest.getPassword())) {
            return Either.left(ErrorResponse.of("Password is invalid"));
        }

        if (userRepository.existsByLogin(userRequest.getLogin())) {
            return Either.left(ErrorResponse.of("Username already exists"));
        }

        final UserEntity entity = UserEntity.of(
                userRequest.getLogin(),
                passwordEncoder.encode(userRequest.getPassword()),
                userRequest.getRole());

        return Either.right(UserResponse.of(userRepository.save(entity).getLogin()));
    }
}
