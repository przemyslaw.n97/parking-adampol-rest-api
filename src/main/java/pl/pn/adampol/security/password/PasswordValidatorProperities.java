package pl.pn.adampol.security.password;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("adampol.parking.security.config.passwordpolicy")
public class PasswordValidatorProperities {

    private int minLength;
    private int maxLength;
}
