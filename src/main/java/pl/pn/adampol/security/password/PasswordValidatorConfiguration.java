package pl.pn.adampol.security.password;

import org.passay.LengthRule;
import org.passay.PasswordValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

@Configuration
public class PasswordValidatorConfiguration {

    @Bean
    PasswordValidator passwordValidator(PasswordValidatorProperities properties) {
        return new PasswordValidator(
                Collections.singletonList(new LengthRule(properties.getMinLength(), properties.getMaxLength())));
    }
}
