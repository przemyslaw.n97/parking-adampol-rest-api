package pl.pn.adampol.security;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Value
@Builder
@ToString(exclude = "password")
public class UserRequest {

    String login;
    String password;
    UserRole role;
}
