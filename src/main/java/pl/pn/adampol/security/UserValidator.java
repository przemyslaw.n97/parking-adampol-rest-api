package pl.pn.adampol.security;

import lombok.RequiredArgsConstructor;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserValidator {

    private final PasswordValidator passwordValidator;

    public boolean validatePassword(String password) {
        final RuleResult validate = passwordValidator.validate(new PasswordData(password));

        return validate.isValid();
    }
}
