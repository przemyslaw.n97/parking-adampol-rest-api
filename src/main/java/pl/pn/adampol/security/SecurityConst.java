package pl.pn.adampol.security;

public interface SecurityConst {

    String TOKEN_SCOPE = "scope";
    String TOKEN_PREFIX = "Bearer ";

}
