package pl.pn.adampol.security;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class LoginRequest {

    String login;
    String password;
}
